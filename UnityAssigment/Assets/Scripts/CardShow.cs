﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CardShow : MonoBehaviour
{
    [SerializeField] private GameObject cardBack;
    [SerializeField] private Controller controller;

    //Sets _id to private so it wont't be accessed within the Unity program.
    private int _id;

    //id is set to public and stores _id within it. 
    public int id
    {
        get { return _id; } //Returns.
    }

    void Start()
    {
        //Calling the below function.
        StartCoroutine(ShowCards()); 
    }

    //This will show the cards for 3 seconds and then hides them.
    IEnumerator ShowCards() 
    {
        cardBack.SetActive(false);
        yield return new WaitForSeconds(3);
        cardBack.SetActive(true);
    }

    //This will get the component as a rendered sprite and equalizes it to the image.
    public void SetCard(int id, Sprite image)
    {
        //_id to id.
        _id = id;
        //Sprite to image.
        GetComponent<SpriteRenderer>().sprite = image; 
    }

    public void OnMouseDown()
    { //On click of a card, the card is shown.Then if the second card is the same as the first card they stay visible, if not the second one is hidden.
        if (cardBack.activeSelf && controller.canReveal)
        { //This if loop keeps everything not revealed.
            cardBack.SetActive(false);
            controller.CardRevealed(this);
        }
    }

    public void Unreveal()
    { //Turns the card.
        cardBack.SetActive(true);
    }
}