﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class End : MonoBehaviour {
    [SerializeField] private TextMesh timebeaten;
    public static float timeTaken;
    // Use this for initialization
    void Start () {
        timebeaten.text = "Time Taken To Complete: " + timeTaken;
    }
	
	// Update is called once per frame
	void Update () {
       
    }
}
