﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class Controller : MonoBehaviour
{
    [SerializeField] private CardShow originalCard;
    [SerializeField] private Sprite[] images;
    [SerializeField] public TextMesh timetext;
    [SerializeField] private TextMesh matches;
    //Creating the variables.
    public const float spacingX = 2.6f; //Spacing between the cards, X-axis.
    public const float spacingY = 2.5f; //Spacing between the cards, Y-axis.
    public const int Rows = 4; //4 rows will be created.
    public const int Columns = 9; //9 columns will be created.

    private CardShow firstcard;
    private CardShow secondcard;

    public AudioClip correctAudio;
    public AudioClip incorrectAudio;
    AudioSource audio;

    private int matchesleft = 18; //Initial matches is set to 18.
    public static float Timer;
    public bool IncreaseTime = true;// A switch for the timer.



   

    public bool canReveal
    {
        get { return secondcard == null; }
    }
    void Update()
    {
        //This if statement is displaing the time on screen.
        if (IncreaseTime == true)
        {
            Timer += Time.deltaTime;
            timetext.text = "Score: " + Timer;
        }
    }
    // Use this for initialization
    void Start()    {
        Vector3 startPos = originalCard.transform.position;

        //Creating the list of cards and shuffling then each time on start of the scene.
        int[] numbers = { 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 14, 14, 15, 15, 16, 16, 17, 17 }; //There is a total of 18 pairs stored in this array.
        numbers = ShuffleArray(numbers);

        //Placing the cards in the grid.
        for (int i = 0; i < Columns; i++)
        { //On the X-axis.

            for (int o = 0; o < Rows; o++)
            { //On the Y-axis.
                CardShow card;
                //Using the original card.
                if (i == 0 && o == 0)
                {
                    card = originalCard;
                }
                else
                {
                    card = Instantiate(originalCard) as CardShow; //The original card is the Audi.
                }

                //Duplication of the original card.
                int index = o * Columns + i;
                int id = numbers[index];
                card.SetCard(id, images[id]);

                float posX = (spacingX * i) + startPos.x; //Spacing the X-axis multiplied by the number of cards.
                float posY = -(spacingY * o) + startPos.y; //Spacing the Y-axis multiplied by the number of cards.
                card.transform.position = new Vector3(posX, posY, startPos.z);
            }
        }
    }

    //Shuffling
    private int[] ShuffleArray(int[] numbers)
    {
        int[] newArray = numbers.Clone() as int[];
        for (int i = 0; i < newArray.Length; i++)
        {
            int tmp = newArray[i];
            int o = Random.Range(i, newArray.Length);
            newArray[i] = newArray[o];
            newArray[o] = tmp;
        }
        return newArray;
    }

    public void CardRevealed(CardShow card)
    {

        if (firstcard == null)
        {
            //This is showing te first card.
            firstcard = card;
        }
        else
        {
            //Checking if bth cards are matching.
            secondcard = card;
            StartCoroutine(CheckMatch());
        }
    }

    private IEnumerator CheckMatch()
    {
        //This if statment is so that the first card has to be the same as the other card, so if the second one is not the you have to find the same card.
        if (firstcard.id != null)
        {
            if (firstcard.id == secondcard.id)
            {
                AudioSource.PlayClipAtPoint(correctAudio, transform.position, 1.0f);
                firstcard = null;

                matchesleft--;
                matches.text = "Matches: " + matchesleft;
            }

            //If cards do not match the second card is hidden only.
            else
            {
                AudioSource.PlayClipAtPoint(incorrectAudio, transform.position, 1.0f);
                //2 second before second card is hidden.
                yield return new WaitForSeconds(2f); 
                                         
                secondcard.Unreveal();

            }


            secondcard = null;
        }
        //When all cards are matched the end scene is loaded and the time is saved.
        if (matchesleft == 0)
        {
            SceneManager.LoadScene("End");
            IncreaseTime = false;
           End.timeTaken = Timer;
        }

    }
}

