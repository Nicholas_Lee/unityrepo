﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour {
    private bool showText = false;
    // Create a bool to say whether to show the button or not

    void OnMouseDown()
    {
        if (!showText)
        {
            showText = true;
            
        }
        // If you clicked the object, set showText to true
    }

    void OnGUI()
    {
        if (showText)
        {

            // If you've clicked the object, show this button
            if (GUI.Button(new Rect(100, 100, 100, 20), "Ready?"))
            {
                // If you click this button, set showText to false
                showText = false;
                SceneManager.LoadScene("Game");
            }
        }
    }
    // Use this for initialization
    void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButton(0))
        {
            OnMouseDown();
            OnGUI();
        }
    }
}
