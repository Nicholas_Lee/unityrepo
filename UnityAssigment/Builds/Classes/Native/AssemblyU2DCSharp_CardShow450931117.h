﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1366199518;
// Controller
struct Controller_t1937198888;

#include "UnityEngine_UnityEngine_MonoBehaviour774292115.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CardShow
struct  CardShow_t450931117  : public MonoBehaviour_t774292115
{
public:
	// UnityEngine.GameObject CardShow::cardBack
	GameObject_t1366199518 * ___cardBack_2;
	// Controller CardShow::controller
	Controller_t1937198888 * ___controller_3;
	// System.Int32 CardShow::_id
	int32_t ____id_4;

public:
	inline static int32_t get_offset_of_cardBack_2() { return static_cast<int32_t>(offsetof(CardShow_t450931117, ___cardBack_2)); }
	inline GameObject_t1366199518 * get_cardBack_2() const { return ___cardBack_2; }
	inline GameObject_t1366199518 ** get_address_of_cardBack_2() { return &___cardBack_2; }
	inline void set_cardBack_2(GameObject_t1366199518 * value)
	{
		___cardBack_2 = value;
		Il2CppCodeGenWriteBarrier(&___cardBack_2, value);
	}

	inline static int32_t get_offset_of_controller_3() { return static_cast<int32_t>(offsetof(CardShow_t450931117, ___controller_3)); }
	inline Controller_t1937198888 * get_controller_3() const { return ___controller_3; }
	inline Controller_t1937198888 ** get_address_of_controller_3() { return &___controller_3; }
	inline void set_controller_3(Controller_t1937198888 * value)
	{
		___controller_3 = value;
		Il2CppCodeGenWriteBarrier(&___controller_3, value);
	}

	inline static int32_t get_offset_of__id_4() { return static_cast<int32_t>(offsetof(CardShow_t450931117, ____id_4)); }
	inline int32_t get__id_4() const { return ____id_4; }
	inline int32_t* get_address_of__id_4() { return &____id_4; }
	inline void set__id_4(int32_t value)
	{
		____id_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
