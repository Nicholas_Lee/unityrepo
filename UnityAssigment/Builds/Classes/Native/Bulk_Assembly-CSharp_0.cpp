﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// <PrivateImplementationDetails>
struct U3CPrivateImplementationDetailsU3E_t1486305142;
// CardShow
struct CardShow_t450931117;
// System.Collections.IEnumerator
struct IEnumerator_t3037427797;
// UnityEngine.Sprite
struct Sprite_t1118776648;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3863917503;
// System.Object
struct Il2CppObject;
// CardShow/<ShowCards>c__Iterator0
struct U3CShowCardsU3Ec__Iterator0_t4256448637;
// Controller
struct Controller_t1937198888;
// System.Int32[]
struct Int32U5BU5D_t3315407976;
// Controller/<CheckMatch>c__Iterator1
struct U3CCheckMatchU3Ec__Iterator1_t4051835777;
// End
struct End_t2340604645;
// Menu
struct Menu_t4261767481;
// PlayAgain
struct PlayAgain_t1655768986;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array4136897760.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137MethodDeclarations.h"
#include "mscorlib_System_Void2799814243.h"
#include "mscorlib_System_Object707969140MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3992678590.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3992678590MethodDeclarations.h"
#include "AssemblyU2DCSharp_CardShow450931117.h"
#include "AssemblyU2DCSharp_CardShow450931117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour774292115MethodDeclarations.h"
#include "mscorlib_System_Int321448170597.h"
#include "UnityEngine_UnityEngine_Coroutine3261918659.h"
#include "AssemblyU2DCSharp_CardShow_U3CShowCardsU3Ec__Itera4256448637MethodDeclarations.h"
#include "AssemblyU2DCSharp_CardShow_U3CShowCardsU3Ec__Itera4256448637.h"
#include "UnityEngine_UnityEngine_Sprite1118776648.h"
#include "UnityEngine_UnityEngine_Component1078601330MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SpriteRenderer3863917503MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SpriteRenderer3863917503.h"
#include "UnityEngine_UnityEngine_Component1078601330.h"
#include "UnityEngine_UnityEngine_GameObject1366199518MethodDeclarations.h"
#include "AssemblyU2DCSharp_Controller1937198888MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1366199518.h"
#include "mscorlib_System_Boolean3143194569.h"
#include "AssemblyU2DCSharp_Controller1937198888.h"
#include "mscorlib_System_Object707969140.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1717981302MethodDeclarations.h"
#include "mscorlib_System_UInt323922122178.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1717981302.h"
#include "mscorlib_System_Single1791520093.h"
#include "mscorlib_System_NotSupportedException3178859535MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException3178859535.h"
#include "UnityEngine_UnityEngine_Object1181371020MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1181371020.h"
#include "UnityEngine_UnityEngine_Time2587606660MethodDeclarations.h"
#include "mscorlib_System_String1967731336MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextMesh3293161711MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextMesh3293161711.h"
#include "mscorlib_System_String1967731336.h"
#include "UnityEngine_UnityEngine_Transform224878301MethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHe1269711593MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector3465617797MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector3465617797.h"
#include "mscorlib_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Transform224878301.h"
#include "mscorlib_System_RuntimeFieldHandle3042581795.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Random265084658MethodDeclarations.h"
#include "mscorlib_System_Array4136897760MethodDeclarations.h"
#include "AssemblyU2DCSharp_Controller_U3CCheckMatchU3Ec__It4051835777MethodDeclarations.h"
#include "AssemblyU2DCSharp_Controller_U3CCheckMatchU3Ec__It4051835777.h"
#include "UnityEngine_UnityEngine_AudioSource585923902MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManage834387985MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioClip3927647597.h"
#include "AssemblyU2DCSharp_End2340604645.h"
#include "AssemblyU2DCSharp_End2340604645MethodDeclarations.h"
#include "AssemblyU2DCSharp_Menu4261767481.h"
#include "AssemblyU2DCSharp_Menu4261767481MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect2553848979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI14333430MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect2553848979.h"
#include "UnityEngine_UnityEngine_Input4173266137MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayAgain1655768986.h"
#include "AssemblyU2DCSharp_PlayAgain1655768986MethodDeclarations.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m2721246802_gshared (Component_t1078601330 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m2721246802(__this, method) ((  Il2CppObject * (*) (Component_t1078601330 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m2721246802_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
#define Component_GetComponent_TisSpriteRenderer_t3863917503_m2178781570(__this, method) ((  SpriteRenderer_t3863917503 * (*) (Component_t1078601330 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m2721246802_gshared)(__this, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m447919519_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m447919519(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m447919519_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Object::Instantiate<CardShow>(!!0)
#define Object_Instantiate_TisCardShow_t450931117_m942920145(__this /* static, unused */, p0, method) ((  CardShow_t450931117 * (*) (Il2CppObject * /* static, unused */, CardShow_t450931117 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m447919519_gshared)(__this /* static, unused */, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void <PrivateImplementationDetails>::.ctor()
extern "C"  void U3CPrivateImplementationDetailsU3E__ctor_m2899642404 (U3CPrivateImplementationDetailsU3E_t1486305142 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$144
extern "C" void U24ArrayTypeU24144_t3992678590_marshal_pinvoke(const U24ArrayTypeU24144_t3992678590& unmarshaled, U24ArrayTypeU24144_t3992678590_marshaled_pinvoke& marshaled)
{
}
extern "C" void U24ArrayTypeU24144_t3992678590_marshal_pinvoke_back(const U24ArrayTypeU24144_t3992678590_marshaled_pinvoke& marshaled, U24ArrayTypeU24144_t3992678590& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$144
extern "C" void U24ArrayTypeU24144_t3992678590_marshal_pinvoke_cleanup(U24ArrayTypeU24144_t3992678590_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$144
extern "C" void U24ArrayTypeU24144_t3992678590_marshal_com(const U24ArrayTypeU24144_t3992678590& unmarshaled, U24ArrayTypeU24144_t3992678590_marshaled_com& marshaled)
{
}
extern "C" void U24ArrayTypeU24144_t3992678590_marshal_com_back(const U24ArrayTypeU24144_t3992678590_marshaled_com& marshaled, U24ArrayTypeU24144_t3992678590& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$144
extern "C" void U24ArrayTypeU24144_t3992678590_marshal_com_cleanup(U24ArrayTypeU24144_t3992678590_marshaled_com& marshaled)
{
}
// System.Void CardShow::.ctor()
extern "C"  void CardShow__ctor_m1370798330 (CardShow_t450931117 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 CardShow::get_id()
extern "C"  int32_t CardShow_get_id_m868565678 (CardShow_t450931117 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__id_4();
		return L_0;
	}
}
// System.Void CardShow::Start()
extern "C"  void CardShow_Start_m3430469306 (CardShow_t450931117 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = CardShow_ShowCards_m4145694826(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator CardShow::ShowCards()
extern Il2CppClass* U3CShowCardsU3Ec__Iterator0_t4256448637_il2cpp_TypeInfo_var;
extern const uint32_t CardShow_ShowCards_m4145694826_MetadataUsageId;
extern "C"  Il2CppObject * CardShow_ShowCards_m4145694826 (CardShow_t450931117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CardShow_ShowCards_m4145694826_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CShowCardsU3Ec__Iterator0_t4256448637 * V_0 = NULL;
	{
		U3CShowCardsU3Ec__Iterator0_t4256448637 * L_0 = (U3CShowCardsU3Ec__Iterator0_t4256448637 *)il2cpp_codegen_object_new(U3CShowCardsU3Ec__Iterator0_t4256448637_il2cpp_TypeInfo_var);
		U3CShowCardsU3Ec__Iterator0__ctor_m1833361428(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CShowCardsU3Ec__Iterator0_t4256448637 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CShowCardsU3Ec__Iterator0_t4256448637 * L_2 = V_0;
		return L_2;
	}
}
// System.Void CardShow::SetCard(System.Int32,UnityEngine.Sprite)
extern const MethodInfo* Component_GetComponent_TisSpriteRenderer_t3863917503_m2178781570_MethodInfo_var;
extern const uint32_t CardShow_SetCard_m2256086209_MetadataUsageId;
extern "C"  void CardShow_SetCard_m2256086209 (CardShow_t450931117 * __this, int32_t ___id0, Sprite_t1118776648 * ___image1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CardShow_SetCard_m2256086209_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___id0;
		__this->set__id_4(L_0);
		SpriteRenderer_t3863917503 * L_1 = Component_GetComponent_TisSpriteRenderer_t3863917503_m2178781570(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t3863917503_m2178781570_MethodInfo_var);
		Sprite_t1118776648 * L_2 = ___image1;
		NullCheck(L_1);
		SpriteRenderer_set_sprite_m617298623(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardShow::OnMouseDown()
extern "C"  void CardShow_OnMouseDown_m1157432862 (CardShow_t450931117 * __this, const MethodInfo* method)
{
	{
		GameObject_t1366199518 * L_0 = __this->get_cardBack_2();
		NullCheck(L_0);
		bool L_1 = GameObject_get_activeSelf_m313590879(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0038;
		}
	}
	{
		Controller_t1937198888 * L_2 = __this->get_controller_3();
		NullCheck(L_2);
		bool L_3 = Controller_get_canReveal_m2709850543(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0038;
		}
	}
	{
		GameObject_t1366199518 * L_4 = __this->get_cardBack_2();
		NullCheck(L_4);
		GameObject_SetActive_m2887581199(L_4, (bool)0, /*hidden argument*/NULL);
		Controller_t1937198888 * L_5 = __this->get_controller_3();
		NullCheck(L_5);
		Controller_CardRevealed_m3279550960(L_5, __this, /*hidden argument*/NULL);
	}

IL_0038:
	{
		return;
	}
}
// System.Void CardShow::Unreveal()
extern "C"  void CardShow_Unreveal_m2518002070 (CardShow_t450931117 * __this, const MethodInfo* method)
{
	{
		GameObject_t1366199518 * L_0 = __this->get_cardBack_2();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardShow/<ShowCards>c__Iterator0::.ctor()
extern "C"  void U3CShowCardsU3Ec__Iterator0__ctor_m1833361428 (U3CShowCardsU3Ec__Iterator0_t4256448637 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object CardShow/<ShowCards>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CShowCardsU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3621482932 (U3CShowCardsU3Ec__Iterator0_t4256448637 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object CardShow/<ShowCards>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CShowCardsU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m907184316 (U3CShowCardsU3Ec__Iterator0_t4256448637 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean CardShow/<ShowCards>c__Iterator0::MoveNext()
extern Il2CppClass* WaitForSeconds_t1717981302_il2cpp_TypeInfo_var;
extern const uint32_t U3CShowCardsU3Ec__Iterator0_MoveNext_m409300712_MetadataUsageId;
extern "C"  bool U3CShowCardsU3Ec__Iterator0_MoveNext_m409300712 (U3CShowCardsU3Ec__Iterator0_t4256448637 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CShowCardsU3Ec__Iterator0_MoveNext_m409300712_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_004e;
		}
	}
	{
		goto IL_0066;
	}

IL_0021:
	{
		CardShow_t450931117 * L_2 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_2);
		GameObject_t1366199518 * L_3 = L_2->get_cardBack_2();
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)0, /*hidden argument*/NULL);
		WaitForSeconds_t1717981302 * L_4 = (WaitForSeconds_t1717981302 *)il2cpp_codegen_object_new(WaitForSeconds_t1717981302_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_4, (3.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_4);
		__this->set_U24PC_0(1);
		goto IL_0068;
	}

IL_004e:
	{
		CardShow_t450931117 * L_5 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_5);
		GameObject_t1366199518 * L_6 = L_5->get_cardBack_2();
		NullCheck(L_6);
		GameObject_SetActive_m2887581199(L_6, (bool)1, /*hidden argument*/NULL);
		__this->set_U24PC_0((-1));
	}

IL_0066:
	{
		return (bool)0;
	}

IL_0068:
	{
		return (bool)1;
	}
	// Dead block : IL_006a: ldloc.1
}
// System.Void CardShow/<ShowCards>c__Iterator0::Dispose()
extern "C"  void U3CShowCardsU3Ec__Iterator0_Dispose_m1706044819 (U3CShowCardsU3Ec__Iterator0_t4256448637 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void CardShow/<ShowCards>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t3178859535_il2cpp_TypeInfo_var;
extern const uint32_t U3CShowCardsU3Ec__Iterator0_Reset_m3522035965_MetadataUsageId;
extern "C"  void U3CShowCardsU3Ec__Iterator0_Reset_m3522035965 (U3CShowCardsU3Ec__Iterator0_t4256448637 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CShowCardsU3Ec__Iterator0_Reset_m3522035965_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t3178859535 * L_0 = (NotSupportedException_t3178859535 *)il2cpp_codegen_object_new(NotSupportedException_t3178859535_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Controller::.ctor()
extern "C"  void Controller__ctor_m2477390111 (Controller_t1937198888 * __this, const MethodInfo* method)
{
	{
		__this->set_matchesleft_15(((int32_t)18));
		__this->set_IncreaseTime_17((bool)1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Controller::get_canReveal()
extern "C"  bool Controller_get_canReveal_m2709850543 (Controller_t1937198888 * __this, const MethodInfo* method)
{
	{
		CardShow_t450931117 * L_0 = __this->get_secondcard_11();
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1181371020 *)NULL, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Controller::Update()
extern Il2CppClass* Controller_t1937198888_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t1791520093_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1756683522;
extern const uint32_t Controller_Update_m2428618086_MetadataUsageId;
extern "C"  void Controller_Update_m2428618086 (Controller_t1937198888 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_Update_m2428618086_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_IncreaseTime_17();
		if (!L_0)
		{
			goto IL_003a;
		}
	}
	{
		float L_1 = ((Controller_t1937198888_StaticFields*)Controller_t1937198888_il2cpp_TypeInfo_var->static_fields)->get_Timer_16();
		float L_2 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Controller_t1937198888_StaticFields*)Controller_t1937198888_il2cpp_TypeInfo_var->static_fields)->set_Timer_16(((float)((float)L_1+(float)L_2)));
		TextMesh_t3293161711 * L_3 = __this->get_timetext_8();
		float L_4 = ((Controller_t1937198888_StaticFields*)Controller_t1937198888_il2cpp_TypeInfo_var->static_fields)->get_Timer_16();
		float L_5 = L_4;
		Il2CppObject * L_6 = Box(Single_t1791520093_il2cpp_TypeInfo_var, &L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1756683522, L_6, /*hidden argument*/NULL);
		NullCheck(L_3);
		TextMesh_set_text_m3390063817(L_3, L_7, /*hidden argument*/NULL);
	}

IL_003a:
	{
		return;
	}
}
// System.Void Controller::Start()
extern Il2CppClass* Int32U5BU5D_t3315407976_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisCardShow_t450931117_m942920145_MethodInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1486305142____U24U24fieldU2D0_0_FieldInfo_var;
extern const uint32_t Controller_Start_m165415507_MetadataUsageId;
extern "C"  void Controller_Start_m165415507 (Controller_t1937198888 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_Start_m165415507_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t465617797  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Int32U5BU5D_t3315407976* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	CardShow_t450931117 * V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	{
		CardShow_t450931117 * L_0 = __this->get_originalCard_6();
		NullCheck(L_0);
		Transform_t224878301 * L_1 = Component_get_transform_m2697483695(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t465617797  L_2 = Transform_get_position_m1104419803(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Int32U5BU5D_t3315407976* L_3 = ((Int32U5BU5D_t3315407976*)SZArrayNew(Int32U5BU5D_t3315407976_il2cpp_TypeInfo_var, (uint32_t)((int32_t)36)));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_3, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1486305142____U24U24fieldU2D0_0_FieldInfo_var), /*hidden argument*/NULL);
		V_1 = L_3;
		Int32U5BU5D_t3315407976* L_4 = V_1;
		Int32U5BU5D_t3315407976* L_5 = Controller_ShuffleArray_m1916119030(__this, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		V_2 = 0;
		goto IL_00d0;
	}

IL_0033:
	{
		V_3 = 0;
		goto IL_00c5;
	}

IL_003a:
	{
		int32_t L_6 = V_2;
		if (L_6)
		{
			goto IL_0053;
		}
	}
	{
		int32_t L_7 = V_3;
		if (L_7)
		{
			goto IL_0053;
		}
	}
	{
		CardShow_t450931117 * L_8 = __this->get_originalCard_6();
		V_4 = L_8;
		goto IL_0060;
	}

IL_0053:
	{
		CardShow_t450931117 * L_9 = __this->get_originalCard_6();
		CardShow_t450931117 * L_10 = Object_Instantiate_TisCardShow_t450931117_m942920145(NULL /*static, unused*/, L_9, /*hidden argument*/Object_Instantiate_TisCardShow_t450931117_m942920145_MethodInfo_var);
		V_4 = L_10;
	}

IL_0060:
	{
		int32_t L_11 = V_3;
		int32_t L_12 = V_2;
		V_5 = ((int32_t)((int32_t)((int32_t)((int32_t)L_11*(int32_t)((int32_t)9)))+(int32_t)L_12));
		Int32U5BU5D_t3315407976* L_13 = V_1;
		int32_t L_14 = V_5;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		V_6 = ((L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15)));
		CardShow_t450931117 * L_16 = V_4;
		int32_t L_17 = V_6;
		SpriteU5BU5D_t4275260569* L_18 = __this->get_images_7();
		int32_t L_19 = V_6;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		int32_t L_20 = L_19;
		NullCheck(L_16);
		CardShow_SetCard_m2256086209(L_16, L_17, ((L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20))), /*hidden argument*/NULL);
		int32_t L_21 = V_2;
		float L_22 = (&V_0)->get_x_1();
		V_7 = ((float)((float)((float)((float)(2.6f)*(float)(((float)((float)L_21)))))+(float)L_22));
		int32_t L_23 = V_3;
		float L_24 = (&V_0)->get_y_2();
		V_8 = ((float)((float)((-((float)((float)(2.5f)*(float)(((float)((float)L_23)))))))+(float)L_24));
		CardShow_t450931117 * L_25 = V_4;
		NullCheck(L_25);
		Transform_t224878301 * L_26 = Component_get_transform_m2697483695(L_25, /*hidden argument*/NULL);
		float L_27 = V_7;
		float L_28 = V_8;
		float L_29 = (&V_0)->get_z_3();
		Vector3_t465617797  L_30;
		memset(&L_30, 0, sizeof(L_30));
		Vector3__ctor_m2638739322(&L_30, L_27, L_28, L_29, /*hidden argument*/NULL);
		NullCheck(L_26);
		Transform_set_position_m2469242620(L_26, L_30, /*hidden argument*/NULL);
		int32_t L_31 = V_3;
		V_3 = ((int32_t)((int32_t)L_31+(int32_t)1));
	}

IL_00c5:
	{
		int32_t L_32 = V_3;
		if ((((int32_t)L_32) < ((int32_t)4)))
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_33 = V_2;
		V_2 = ((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_00d0:
	{
		int32_t L_34 = V_2;
		if ((((int32_t)L_34) < ((int32_t)((int32_t)9))))
		{
			goto IL_0033;
		}
	}
	{
		return;
	}
}
// System.Int32[] Controller::ShuffleArray(System.Int32[])
extern Il2CppClass* Int32U5BU5D_t3315407976_il2cpp_TypeInfo_var;
extern const uint32_t Controller_ShuffleArray_m1916119030_MetadataUsageId;
extern "C"  Int32U5BU5D_t3315407976* Controller_ShuffleArray_m1916119030 (Controller_t1937198888 * __this, Int32U5BU5D_t3315407976* ___numbers0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_ShuffleArray_m1916119030_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Int32U5BU5D_t3315407976* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		Int32U5BU5D_t3315407976* L_0 = ___numbers0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(5 /* System.Object System.Array::Clone() */, (Il2CppArray *)(Il2CppArray *)L_0);
		V_0 = ((Int32U5BU5D_t3315407976*)IsInst(L_1, Int32U5BU5D_t3315407976_il2cpp_TypeInfo_var));
		V_1 = 0;
		goto IL_002f;
	}

IL_0013:
	{
		Int32U5BU5D_t3315407976* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_2 = ((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4)));
		int32_t L_5 = V_1;
		Int32U5BU5D_t3315407976* L_6 = V_0;
		NullCheck(L_6);
		int32_t L_7 = Random_Range_m694320887(NULL /*static, unused*/, L_5, (((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length)))), /*hidden argument*/NULL);
		V_3 = L_7;
		Int32U5BU5D_t3315407976* L_8 = V_0;
		int32_t L_9 = V_1;
		Int32U5BU5D_t3315407976* L_10 = V_0;
		int32_t L_11 = V_3;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (int32_t)((L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12))));
		Int32U5BU5D_t3315407976* L_13 = V_0;
		int32_t L_14 = V_3;
		int32_t L_15 = V_2;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (int32_t)L_15);
		int32_t L_16 = V_1;
		V_1 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_002f:
	{
		int32_t L_17 = V_1;
		Int32U5BU5D_t3315407976* L_18 = V_0;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		Int32U5BU5D_t3315407976* L_19 = V_0;
		return L_19;
	}
}
// System.Void Controller::CardRevealed(CardShow)
extern "C"  void Controller_CardRevealed_m3279550960 (Controller_t1937198888 * __this, CardShow_t450931117 * ___card0, const MethodInfo* method)
{
	{
		CardShow_t450931117 * L_0 = __this->get_firstcard_10();
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1181371020 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		CardShow_t450931117 * L_2 = ___card0;
		__this->set_firstcard_10(L_2);
		goto IL_0031;
	}

IL_001d:
	{
		CardShow_t450931117 * L_3 = ___card0;
		__this->set_secondcard_11(L_3);
		Il2CppObject * L_4 = Controller_CheckMatch_m1605907186(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_4, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Collections.IEnumerator Controller::CheckMatch()
extern Il2CppClass* U3CCheckMatchU3Ec__Iterator1_t4051835777_il2cpp_TypeInfo_var;
extern const uint32_t Controller_CheckMatch_m1605907186_MetadataUsageId;
extern "C"  Il2CppObject * Controller_CheckMatch_m1605907186 (Controller_t1937198888 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_CheckMatch_m1605907186_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CCheckMatchU3Ec__Iterator1_t4051835777 * V_0 = NULL;
	{
		U3CCheckMatchU3Ec__Iterator1_t4051835777 * L_0 = (U3CCheckMatchU3Ec__Iterator1_t4051835777 *)il2cpp_codegen_object_new(U3CCheckMatchU3Ec__Iterator1_t4051835777_il2cpp_TypeInfo_var);
		U3CCheckMatchU3Ec__Iterator1__ctor_m3136006115(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CCheckMatchU3Ec__Iterator1_t4051835777 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CCheckMatchU3Ec__Iterator1_t4051835777 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Controller/<CheckMatch>c__Iterator1::.ctor()
extern "C"  void U3CCheckMatchU3Ec__Iterator1__ctor_m3136006115 (U3CCheckMatchU3Ec__Iterator1_t4051835777 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Controller/<CheckMatch>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckMatchU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m204460229 (U3CCheckMatchU3Ec__Iterator1_t4051835777 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object Controller/<CheckMatch>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckMatchU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m330069869 (U3CCheckMatchU3Ec__Iterator1_t4051835777 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean Controller/<CheckMatch>c__Iterator1::MoveNext()
extern Il2CppClass* Int32_t1448170597_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForSeconds_t1717981302_il2cpp_TypeInfo_var;
extern Il2CppClass* Controller_t1937198888_il2cpp_TypeInfo_var;
extern Il2CppClass* End_t2340604645_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3183125567;
extern Il2CppCodeGenString* _stringLiteral3068683163;
extern const uint32_t U3CCheckMatchU3Ec__Iterator1_MoveNext_m1429431989_MetadataUsageId;
extern "C"  bool U3CCheckMatchU3Ec__Iterator1_MoveNext_m1429431989 (U3CCheckMatchU3Ec__Iterator1_t4051835777 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCheckMatchU3Ec__Iterator1_MoveNext_m1429431989_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00fa;
		}
	}
	{
		goto IL_014d;
	}

IL_0021:
	{
		Controller_t1937198888 * L_2 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_2);
		CardShow_t450931117 * L_3 = L_2->get_firstcard_10();
		NullCheck(L_3);
		int32_t L_4 = CardShow_get_id_m868565678(L_3, /*hidden argument*/NULL);
		Controller_t1937198888 * L_5 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_5);
		CardShow_t450931117 * L_6 = L_5->get_secondcard_11();
		NullCheck(L_6);
		int32_t L_7 = CardShow_get_id_m868565678(L_6, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)L_7))))
		{
			goto IL_00b9;
		}
	}
	{
		Controller_t1937198888 * L_8 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_8);
		AudioClip_t3927647597 * L_9 = L_8->get_correctAudio_12();
		Controller_t1937198888 * L_10 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_10);
		Transform_t224878301 * L_11 = Component_get_transform_m2697483695(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t465617797  L_12 = Transform_get_position_m1104419803(L_11, /*hidden argument*/NULL);
		AudioSource_PlayClipAtPoint_m1469997862(NULL /*static, unused*/, L_9, L_12, (1.0f), /*hidden argument*/NULL);
		Controller_t1937198888 * L_13 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_13);
		L_13->set_firstcard_10((CardShow_t450931117 *)NULL);
		Controller_t1937198888 * L_14 = __this->get_U3CU3Ef__this_2();
		Controller_t1937198888 * L_15 = L_14;
		NullCheck(L_15);
		int32_t L_16 = L_15->get_matchesleft_15();
		NullCheck(L_15);
		L_15->set_matchesleft_15(((int32_t)((int32_t)L_16-(int32_t)1)));
		Controller_t1937198888 * L_17 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_17);
		TextMesh_t3293161711 * L_18 = L_17->get_matches_9();
		Controller_t1937198888 * L_19 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_19);
		int32_t L_20 = L_19->get_matchesleft_15();
		int32_t L_21 = L_20;
		Il2CppObject * L_22 = Box(Int32_t1448170597_il2cpp_TypeInfo_var, &L_21);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral3183125567, L_22, /*hidden argument*/NULL);
		NullCheck(L_18);
		TextMesh_set_text_m3390063817(L_18, L_23, /*hidden argument*/NULL);
		goto IL_010a;
	}

IL_00b9:
	{
		Controller_t1937198888 * L_24 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_24);
		AudioClip_t3927647597 * L_25 = L_24->get_incorrectAudio_13();
		Controller_t1937198888 * L_26 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_26);
		Transform_t224878301 * L_27 = Component_get_transform_m2697483695(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		Vector3_t465617797  L_28 = Transform_get_position_m1104419803(L_27, /*hidden argument*/NULL);
		AudioSource_PlayClipAtPoint_m1469997862(NULL /*static, unused*/, L_25, L_28, (1.0f), /*hidden argument*/NULL);
		WaitForSeconds_t1717981302 * L_29 = (WaitForSeconds_t1717981302 *)il2cpp_codegen_object_new(WaitForSeconds_t1717981302_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_29, (2.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_29);
		__this->set_U24PC_0(1);
		goto IL_014f;
	}

IL_00fa:
	{
		Controller_t1937198888 * L_30 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_30);
		CardShow_t450931117 * L_31 = L_30->get_secondcard_11();
		NullCheck(L_31);
		CardShow_Unreveal_m2518002070(L_31, /*hidden argument*/NULL);
	}

IL_010a:
	{
		Controller_t1937198888 * L_32 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_32);
		L_32->set_secondcard_11((CardShow_t450931117 *)NULL);
		Controller_t1937198888 * L_33 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_33);
		int32_t L_34 = L_33->get_matchesleft_15();
		if (L_34)
		{
			goto IL_0146;
		}
	}
	{
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral3068683163, /*hidden argument*/NULL);
		Controller_t1937198888 * L_35 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_35);
		L_35->set_IncreaseTime_17((bool)0);
		float L_36 = ((Controller_t1937198888_StaticFields*)Controller_t1937198888_il2cpp_TypeInfo_var->static_fields)->get_Timer_16();
		((End_t2340604645_StaticFields*)End_t2340604645_il2cpp_TypeInfo_var->static_fields)->set_timeTaken_3(L_36);
	}

IL_0146:
	{
		__this->set_U24PC_0((-1));
	}

IL_014d:
	{
		return (bool)0;
	}

IL_014f:
	{
		return (bool)1;
	}
	// Dead block : IL_0151: ldloc.1
}
// System.Void Controller/<CheckMatch>c__Iterator1::Dispose()
extern "C"  void U3CCheckMatchU3Ec__Iterator1_Dispose_m2289975758 (U3CCheckMatchU3Ec__Iterator1_t4051835777 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void Controller/<CheckMatch>c__Iterator1::Reset()
extern Il2CppClass* NotSupportedException_t3178859535_il2cpp_TypeInfo_var;
extern const uint32_t U3CCheckMatchU3Ec__Iterator1_Reset_m1092331264_MetadataUsageId;
extern "C"  void U3CCheckMatchU3Ec__Iterator1_Reset_m1092331264 (U3CCheckMatchU3Ec__Iterator1_t4051835777 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCheckMatchU3Ec__Iterator1_Reset_m1092331264_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t3178859535 * L_0 = (NotSupportedException_t3178859535 *)il2cpp_codegen_object_new(NotSupportedException_t3178859535_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void End::.ctor()
extern "C"  void End__ctor_m423682380 (End_t2340604645 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void End::Start()
extern Il2CppClass* End_t2340604645_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t1791520093_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1968660154;
extern const uint32_t End_Start_m1122033132_MetadataUsageId;
extern "C"  void End_Start_m1122033132 (End_t2340604645 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (End_Start_m1122033132_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TextMesh_t3293161711 * L_0 = __this->get_timebeaten_2();
		float L_1 = ((End_t2340604645_StaticFields*)End_t2340604645_il2cpp_TypeInfo_var->static_fields)->get_timeTaken_3();
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t1791520093_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1968660154, L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		TextMesh_set_text_m3390063817(L_0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void End::Update()
extern "C"  void End_Update_m1276124681 (End_t2340604645 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Menu::.ctor()
extern "C"  void Menu__ctor_m2507984538 (Menu_t4261767481 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Menu::OnMouseDown()
extern "C"  void Menu_OnMouseDown_m3235756386 (Menu_t4261767481 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_showText_2();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		__this->set_showText_2((bool)1);
	}

IL_0012:
	{
		return;
	}
}
// System.Void Menu::OnGUI()
extern Il2CppClass* GUI_t14333430_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2141530294;
extern Il2CppCodeGenString* _stringLiteral2328219732;
extern const uint32_t Menu_OnGUI_m4231420370_MetadataUsageId;
extern "C"  void Menu_OnGUI_m4231420370 (Menu_t4261767481 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Menu_OnGUI_m4231420370_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_showText_2();
		if (!L_0)
		{
			goto IL_0044;
		}
	}
	{
		Rect_t2553848979  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Rect__ctor_m1220545469(&L_1, (100.0f), (100.0f), (100.0f), (20.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t14333430_il2cpp_TypeInfo_var);
		bool L_2 = GUI_Button_m3054448581(NULL /*static, unused*/, L_1, _stringLiteral2141530294, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0044;
		}
	}
	{
		__this->set_showText_2((bool)0);
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral2328219732, /*hidden argument*/NULL);
	}

IL_0044:
	{
		return;
	}
}
// System.Void Menu::Start()
extern "C"  void Menu_Start_m3632225306 (Menu_t4261767481 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Menu::Update()
extern Il2CppClass* Input_t4173266137_il2cpp_TypeInfo_var;
extern const uint32_t Menu_Update_m4082554617_MetadataUsageId;
extern "C"  void Menu_Update_m4082554617 (Menu_t4261767481 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Menu_Update_m4082554617_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4173266137_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButton_m464100923(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Menu_OnMouseDown_m3235756386(__this, /*hidden argument*/NULL);
		Menu_OnGUI_m4231420370(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void PlayAgain::.ctor()
extern "C"  void PlayAgain__ctor_m1254445871 (PlayAgain_t1655768986 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayAgain::Start()
extern "C"  void PlayAgain_Start_m588511243 (PlayAgain_t1655768986 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void PlayAgain::Update()
extern Il2CppClass* Input_t4173266137_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral694878703;
extern const uint32_t PlayAgain_Update_m3663889988_MetadataUsageId;
extern "C"  void PlayAgain_Update_m3663889988 (PlayAgain_t1655768986 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayAgain_Update_m3663889988_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4173266137_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButton_m464100923(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral694878703, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
