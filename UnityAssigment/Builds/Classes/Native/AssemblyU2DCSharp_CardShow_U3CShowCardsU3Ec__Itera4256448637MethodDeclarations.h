﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CardShow/<ShowCards>c__Iterator0
struct U3CShowCardsU3Ec__Iterator0_t4256448637;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void CardShow/<ShowCards>c__Iterator0::.ctor()
extern "C"  void U3CShowCardsU3Ec__Iterator0__ctor_m1833361428 (U3CShowCardsU3Ec__Iterator0_t4256448637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CardShow/<ShowCards>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CShowCardsU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3621482932 (U3CShowCardsU3Ec__Iterator0_t4256448637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CardShow/<ShowCards>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CShowCardsU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m907184316 (U3CShowCardsU3Ec__Iterator0_t4256448637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CardShow/<ShowCards>c__Iterator0::MoveNext()
extern "C"  bool U3CShowCardsU3Ec__Iterator0_MoveNext_m409300712 (U3CShowCardsU3Ec__Iterator0_t4256448637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardShow/<ShowCards>c__Iterator0::Dispose()
extern "C"  void U3CShowCardsU3Ec__Iterator0_Dispose_m1706044819 (U3CShowCardsU3Ec__Iterator0_t4256448637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardShow/<ShowCards>c__Iterator0::Reset()
extern "C"  void U3CShowCardsU3Ec__Iterator0_Reset_m3522035965 (U3CShowCardsU3Ec__Iterator0_t4256448637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
