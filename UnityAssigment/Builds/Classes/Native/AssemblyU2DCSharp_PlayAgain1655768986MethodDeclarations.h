﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayAgain
struct PlayAgain_t1655768986;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayAgain::.ctor()
extern "C"  void PlayAgain__ctor_m1254445871 (PlayAgain_t1655768986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayAgain::Start()
extern "C"  void PlayAgain_Start_m588511243 (PlayAgain_t1655768986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayAgain::Update()
extern "C"  void PlayAgain_Update_m3663889988 (PlayAgain_t1655768986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
