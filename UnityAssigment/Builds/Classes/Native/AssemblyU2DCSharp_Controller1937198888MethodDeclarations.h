﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Controller
struct Controller_t1937198888;
// System.Int32[]
struct Int32U5BU5D_t3315407976;
// CardShow
struct CardShow_t450931117;
// System.Collections.IEnumerator
struct IEnumerator_t3037427797;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CardShow450931117.h"

// System.Void Controller::.ctor()
extern "C"  void Controller__ctor_m2477390111 (Controller_t1937198888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Controller::get_canReveal()
extern "C"  bool Controller_get_canReveal_m2709850543 (Controller_t1937198888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::Update()
extern "C"  void Controller_Update_m2428618086 (Controller_t1937198888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::Start()
extern "C"  void Controller_Start_m165415507 (Controller_t1937198888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] Controller::ShuffleArray(System.Int32[])
extern "C"  Int32U5BU5D_t3315407976* Controller_ShuffleArray_m1916119030 (Controller_t1937198888 * __this, Int32U5BU5D_t3315407976* ___numbers0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::CardRevealed(CardShow)
extern "C"  void Controller_CardRevealed_m3279550960 (Controller_t1937198888 * __this, CardShow_t450931117 * ___card0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Controller::CheckMatch()
extern "C"  Il2CppObject * Controller_CheckMatch_m1605907186 (Controller_t1937198888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
