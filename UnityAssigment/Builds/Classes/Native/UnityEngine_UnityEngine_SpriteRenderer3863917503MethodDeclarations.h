﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3863917503;
// UnityEngine.Sprite
struct Sprite_t1118776648;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Sprite1118776648.h"

// System.Void UnityEngine.SpriteRenderer::set_sprite(UnityEngine.Sprite)
extern "C"  void SpriteRenderer_set_sprite_m617298623 (SpriteRenderer_t3863917503 * __this, Sprite_t1118776648 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpriteRenderer::SetSprite_INTERNAL(UnityEngine.Sprite)
extern "C"  void SpriteRenderer_SetSprite_INTERNAL_m2338909670 (SpriteRenderer_t3863917503 * __this, Sprite_t1118776648 * ___sprite0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
