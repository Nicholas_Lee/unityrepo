﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour774292115.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Menu
struct  Menu_t4261767481  : public MonoBehaviour_t774292115
{
public:
	// System.Boolean Menu::showText
	bool ___showText_2;

public:
	inline static int32_t get_offset_of_showText_2() { return static_cast<int32_t>(offsetof(Menu_t4261767481, ___showText_2)); }
	inline bool get_showText_2() const { return ___showText_2; }
	inline bool* get_address_of_showText_2() { return &___showText_2; }
	inline void set_showText_2(bool value)
	{
		___showText_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
