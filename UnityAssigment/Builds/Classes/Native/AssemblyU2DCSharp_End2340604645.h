﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.TextMesh
struct TextMesh_t3293161711;

#include "UnityEngine_UnityEngine_MonoBehaviour774292115.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// End
struct  End_t2340604645  : public MonoBehaviour_t774292115
{
public:
	// UnityEngine.TextMesh End::timebeaten
	TextMesh_t3293161711 * ___timebeaten_2;

public:
	inline static int32_t get_offset_of_timebeaten_2() { return static_cast<int32_t>(offsetof(End_t2340604645, ___timebeaten_2)); }
	inline TextMesh_t3293161711 * get_timebeaten_2() const { return ___timebeaten_2; }
	inline TextMesh_t3293161711 ** get_address_of_timebeaten_2() { return &___timebeaten_2; }
	inline void set_timebeaten_2(TextMesh_t3293161711 * value)
	{
		___timebeaten_2 = value;
		Il2CppCodeGenWriteBarrier(&___timebeaten_2, value);
	}
};

struct End_t2340604645_StaticFields
{
public:
	// System.Single End::timeTaken
	float ___timeTaken_3;

public:
	inline static int32_t get_offset_of_timeTaken_3() { return static_cast<int32_t>(offsetof(End_t2340604645_StaticFields, ___timeTaken_3)); }
	inline float get_timeTaken_3() const { return ___timeTaken_3; }
	inline float* get_address_of_timeTaken_3() { return &___timeTaken_3; }
	inline void set_timeTaken_3(float value)
	{
		___timeTaken_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
