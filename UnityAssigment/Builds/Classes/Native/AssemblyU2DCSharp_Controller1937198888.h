﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CardShow
struct CardShow_t450931117;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t4275260569;
// UnityEngine.TextMesh
struct TextMesh_t3293161711;
// UnityEngine.AudioClip
struct AudioClip_t3927647597;
// UnityEngine.AudioSource
struct AudioSource_t585923902;

#include "UnityEngine_UnityEngine_MonoBehaviour774292115.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Controller
struct  Controller_t1937198888  : public MonoBehaviour_t774292115
{
public:
	// CardShow Controller::originalCard
	CardShow_t450931117 * ___originalCard_6;
	// UnityEngine.Sprite[] Controller::images
	SpriteU5BU5D_t4275260569* ___images_7;
	// UnityEngine.TextMesh Controller::timetext
	TextMesh_t3293161711 * ___timetext_8;
	// UnityEngine.TextMesh Controller::matches
	TextMesh_t3293161711 * ___matches_9;
	// CardShow Controller::firstcard
	CardShow_t450931117 * ___firstcard_10;
	// CardShow Controller::secondcard
	CardShow_t450931117 * ___secondcard_11;
	// UnityEngine.AudioClip Controller::correctAudio
	AudioClip_t3927647597 * ___correctAudio_12;
	// UnityEngine.AudioClip Controller::incorrectAudio
	AudioClip_t3927647597 * ___incorrectAudio_13;
	// UnityEngine.AudioSource Controller::audio
	AudioSource_t585923902 * ___audio_14;
	// System.Int32 Controller::matchesleft
	int32_t ___matchesleft_15;
	// System.Boolean Controller::IncreaseTime
	bool ___IncreaseTime_17;

public:
	inline static int32_t get_offset_of_originalCard_6() { return static_cast<int32_t>(offsetof(Controller_t1937198888, ___originalCard_6)); }
	inline CardShow_t450931117 * get_originalCard_6() const { return ___originalCard_6; }
	inline CardShow_t450931117 ** get_address_of_originalCard_6() { return &___originalCard_6; }
	inline void set_originalCard_6(CardShow_t450931117 * value)
	{
		___originalCard_6 = value;
		Il2CppCodeGenWriteBarrier(&___originalCard_6, value);
	}

	inline static int32_t get_offset_of_images_7() { return static_cast<int32_t>(offsetof(Controller_t1937198888, ___images_7)); }
	inline SpriteU5BU5D_t4275260569* get_images_7() const { return ___images_7; }
	inline SpriteU5BU5D_t4275260569** get_address_of_images_7() { return &___images_7; }
	inline void set_images_7(SpriteU5BU5D_t4275260569* value)
	{
		___images_7 = value;
		Il2CppCodeGenWriteBarrier(&___images_7, value);
	}

	inline static int32_t get_offset_of_timetext_8() { return static_cast<int32_t>(offsetof(Controller_t1937198888, ___timetext_8)); }
	inline TextMesh_t3293161711 * get_timetext_8() const { return ___timetext_8; }
	inline TextMesh_t3293161711 ** get_address_of_timetext_8() { return &___timetext_8; }
	inline void set_timetext_8(TextMesh_t3293161711 * value)
	{
		___timetext_8 = value;
		Il2CppCodeGenWriteBarrier(&___timetext_8, value);
	}

	inline static int32_t get_offset_of_matches_9() { return static_cast<int32_t>(offsetof(Controller_t1937198888, ___matches_9)); }
	inline TextMesh_t3293161711 * get_matches_9() const { return ___matches_9; }
	inline TextMesh_t3293161711 ** get_address_of_matches_9() { return &___matches_9; }
	inline void set_matches_9(TextMesh_t3293161711 * value)
	{
		___matches_9 = value;
		Il2CppCodeGenWriteBarrier(&___matches_9, value);
	}

	inline static int32_t get_offset_of_firstcard_10() { return static_cast<int32_t>(offsetof(Controller_t1937198888, ___firstcard_10)); }
	inline CardShow_t450931117 * get_firstcard_10() const { return ___firstcard_10; }
	inline CardShow_t450931117 ** get_address_of_firstcard_10() { return &___firstcard_10; }
	inline void set_firstcard_10(CardShow_t450931117 * value)
	{
		___firstcard_10 = value;
		Il2CppCodeGenWriteBarrier(&___firstcard_10, value);
	}

	inline static int32_t get_offset_of_secondcard_11() { return static_cast<int32_t>(offsetof(Controller_t1937198888, ___secondcard_11)); }
	inline CardShow_t450931117 * get_secondcard_11() const { return ___secondcard_11; }
	inline CardShow_t450931117 ** get_address_of_secondcard_11() { return &___secondcard_11; }
	inline void set_secondcard_11(CardShow_t450931117 * value)
	{
		___secondcard_11 = value;
		Il2CppCodeGenWriteBarrier(&___secondcard_11, value);
	}

	inline static int32_t get_offset_of_correctAudio_12() { return static_cast<int32_t>(offsetof(Controller_t1937198888, ___correctAudio_12)); }
	inline AudioClip_t3927647597 * get_correctAudio_12() const { return ___correctAudio_12; }
	inline AudioClip_t3927647597 ** get_address_of_correctAudio_12() { return &___correctAudio_12; }
	inline void set_correctAudio_12(AudioClip_t3927647597 * value)
	{
		___correctAudio_12 = value;
		Il2CppCodeGenWriteBarrier(&___correctAudio_12, value);
	}

	inline static int32_t get_offset_of_incorrectAudio_13() { return static_cast<int32_t>(offsetof(Controller_t1937198888, ___incorrectAudio_13)); }
	inline AudioClip_t3927647597 * get_incorrectAudio_13() const { return ___incorrectAudio_13; }
	inline AudioClip_t3927647597 ** get_address_of_incorrectAudio_13() { return &___incorrectAudio_13; }
	inline void set_incorrectAudio_13(AudioClip_t3927647597 * value)
	{
		___incorrectAudio_13 = value;
		Il2CppCodeGenWriteBarrier(&___incorrectAudio_13, value);
	}

	inline static int32_t get_offset_of_audio_14() { return static_cast<int32_t>(offsetof(Controller_t1937198888, ___audio_14)); }
	inline AudioSource_t585923902 * get_audio_14() const { return ___audio_14; }
	inline AudioSource_t585923902 ** get_address_of_audio_14() { return &___audio_14; }
	inline void set_audio_14(AudioSource_t585923902 * value)
	{
		___audio_14 = value;
		Il2CppCodeGenWriteBarrier(&___audio_14, value);
	}

	inline static int32_t get_offset_of_matchesleft_15() { return static_cast<int32_t>(offsetof(Controller_t1937198888, ___matchesleft_15)); }
	inline int32_t get_matchesleft_15() const { return ___matchesleft_15; }
	inline int32_t* get_address_of_matchesleft_15() { return &___matchesleft_15; }
	inline void set_matchesleft_15(int32_t value)
	{
		___matchesleft_15 = value;
	}

	inline static int32_t get_offset_of_IncreaseTime_17() { return static_cast<int32_t>(offsetof(Controller_t1937198888, ___IncreaseTime_17)); }
	inline bool get_IncreaseTime_17() const { return ___IncreaseTime_17; }
	inline bool* get_address_of_IncreaseTime_17() { return &___IncreaseTime_17; }
	inline void set_IncreaseTime_17(bool value)
	{
		___IncreaseTime_17 = value;
	}
};

struct Controller_t1937198888_StaticFields
{
public:
	// System.Single Controller::Timer
	float ___Timer_16;

public:
	inline static int32_t get_offset_of_Timer_16() { return static_cast<int32_t>(offsetof(Controller_t1937198888_StaticFields, ___Timer_16)); }
	inline float get_Timer_16() const { return ___Timer_16; }
	inline float* get_address_of_Timer_16() { return &___Timer_16; }
	inline void set_Timer_16(float value)
	{
		___Timer_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
