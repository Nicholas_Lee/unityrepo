﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CardShow
struct CardShow_t450931117;
// System.Collections.IEnumerator
struct IEnumerator_t3037427797;
// UnityEngine.Sprite
struct Sprite_t1118776648;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Sprite1118776648.h"

// System.Void CardShow::.ctor()
extern "C"  void CardShow__ctor_m1370798330 (CardShow_t450931117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CardShow::get_id()
extern "C"  int32_t CardShow_get_id_m868565678 (CardShow_t450931117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardShow::Start()
extern "C"  void CardShow_Start_m3430469306 (CardShow_t450931117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CardShow::ShowCards()
extern "C"  Il2CppObject * CardShow_ShowCards_m4145694826 (CardShow_t450931117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardShow::SetCard(System.Int32,UnityEngine.Sprite)
extern "C"  void CardShow_SetCard_m2256086209 (CardShow_t450931117 * __this, int32_t ___id0, Sprite_t1118776648 * ___image1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardShow::OnMouseDown()
extern "C"  void CardShow_OnMouseDown_m1157432862 (CardShow_t450931117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardShow::Unreveal()
extern "C"  void CardShow_Unreveal_m2518002070 (CardShow_t450931117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
