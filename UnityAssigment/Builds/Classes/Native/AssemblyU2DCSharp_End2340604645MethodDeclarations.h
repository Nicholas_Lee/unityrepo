﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// End
struct End_t2340604645;

#include "codegen/il2cpp-codegen.h"

// System.Void End::.ctor()
extern "C"  void End__ctor_m423682380 (End_t2340604645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void End::Start()
extern "C"  void End_Start_m1122033132 (End_t2340604645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void End::Update()
extern "C"  void End_Update_m1276124681 (End_t2340604645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
