﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Controller/<CheckMatch>c__Iterator1
struct U3CCheckMatchU3Ec__Iterator1_t4051835777;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Controller/<CheckMatch>c__Iterator1::.ctor()
extern "C"  void U3CCheckMatchU3Ec__Iterator1__ctor_m3136006115 (U3CCheckMatchU3Ec__Iterator1_t4051835777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Controller/<CheckMatch>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckMatchU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m204460229 (U3CCheckMatchU3Ec__Iterator1_t4051835777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Controller/<CheckMatch>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckMatchU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m330069869 (U3CCheckMatchU3Ec__Iterator1_t4051835777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Controller/<CheckMatch>c__Iterator1::MoveNext()
extern "C"  bool U3CCheckMatchU3Ec__Iterator1_MoveNext_m1429431989 (U3CCheckMatchU3Ec__Iterator1_t4051835777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller/<CheckMatch>c__Iterator1::Dispose()
extern "C"  void U3CCheckMatchU3Ec__Iterator1_Dispose_m2289975758 (U3CCheckMatchU3Ec__Iterator1_t4051835777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller/<CheckMatch>c__Iterator1::Reset()
extern "C"  void U3CCheckMatchU3Ec__Iterator1_Reset_m1092331264 (U3CCheckMatchU3Ec__Iterator1_t4051835777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
